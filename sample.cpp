#ifndef __PROGTEST__

#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <string>
#include <string_view>
#include <vector>

#include <openssl/evp.h>
#include <openssl/rand.h>

using namespace std;

#endif /* __PROGTEST__ */
std::string convertToHex ( const char * src, size_t srcLen ) {
    std::stringstream ss;
    for ( size_t i = 0; i < srcLen; i++ )
        ss << std::setfill('0') << std::setw(2) << std::hex <<  ( unsigned int ) ( unsigned char ) src[i];
    return ss.str();
}
void sendBytesAsHex ( const char * src, std::string &dst, size_t size ) {
    std::string hexMsg = convertToHex(src, size);
    dst =  hexMsg ;
}


static const auto HASH_SIZE = EVP_MAX_MD_SIZE;

bool foundMessage ( const int bits, const unsigned char * hash, const size_t hashLength ) {
    if ( ! hash || hashLength <= 0 || bits < 0  )
        return false;
    int zeroBitCounter = 0;
    for ( size_t i = 0; i < hashLength; i++ ) {
        unsigned char word = hash[i];
        for ( int j = 0; j < 8; j++ ) {
            if ( ( word & 0b10000000 ) != 0 )
                return zeroBitCounter >= bits;
            zeroBitCounter++;
            word <<= 1;
        }
    }
    return zeroBitCounter >= bits;
}


int findHashEx(int bits, std::string &message, std::string &hash, std::string_view hashFunction) {
    const EVP_MD * hashFuncType = EVP_get_digestbyname(hashFunction.data());
    if ( ! hashFuncType || bits < 0 || bits > EVP_MD_size(hashFuncType))
        return 0;
    EVP_MD_CTX *ctx;
    if ( ctx = EVP_MD_CTX_new(); ctx == NULL ) {
        printf("Context creation failed.\n");
        return false;
    }
    unsigned char hash_arr[EVP_MAX_MD_SIZE];

    /* such size because the hash is going to be recycled as the new message */
    char mess [HASH_SIZE] = {0};
    RAND_bytes (reinterpret_cast<unsigned char *>(mess), HASH_SIZE );


    unsigned int hash_length = 0;

    while ( true ) {

        if ( ! EVP_DigestInit_ex( ctx, hashFuncType, NULL ) ) {
            printf( "Digest setup failed.\n");
            EVP_MD_CTX_free(ctx); // destroy the context
            return 0;
        }

        if ( ! EVP_DigestUpdate ( ctx, mess, HASH_SIZE ) ) {
            printf("Failed fill message.\n");
            EVP_MD_CTX_free(ctx); // destroy the context
            return 0;
        }

        if ( ! EVP_DigestFinal_ex (ctx, hash_arr,&hash_length) ) {
            printf("Failed to finalize the hash.\n");
            EVP_MD_CTX_free(ctx); // destroy the context
            return 0;
        }

        if ( foundMessage ( bits, hash_arr, hash_length ) )
            break;
        memcpy ( mess, hash_arr, HASH_SIZE ); // use the next message as the previously generated hash
    }

    sendBytesAsHex(mess, message, HASH_SIZE );
    sendBytesAsHex (reinterpret_cast<const char *>(hash_arr), hash, hash_length );
    EVP_MD_CTX_free(ctx); // destroy the context
    return 1;
}
int findHash(int bits, std::string &message, std::string &hash) {
    return findHashEx(bits,message,hash,"sha512");
}

#ifndef __PROGTEST__

int main(void) {
    string hash, message;

    assert(findHash(0, message, hash) == 1);
    assert(!message.empty() && !hash.empty() && checkHash(0, hash));
    message.clear();
    hash.clear();
    assert(findHash(1, message, hash) == 1);
    assert(!message.empty() && !hash.empty() && checkHash(1, hash));
    message.clear();
    hash.clear();
    assert(findHash(2, message, hash) == 1);
    assert(!message.empty() && !hash.empty() && checkHash(2, hash));
    message.clear();
    hash.clear();
    assert(findHash(3, message, hash) == 1);
    assert(!message.empty() && !hash.empty() && checkHash(3, hash));
    message.clear();
    hash.clear();
    assert(findHash(-1, message, hash) == 0);

    return EXIT_SUCCESS;
}

#endif /* __PROGTEST__ */

